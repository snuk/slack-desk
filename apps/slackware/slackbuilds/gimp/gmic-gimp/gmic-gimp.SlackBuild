#!/bin/sh

# Slackware build script for gmic-gimp 

# Copyright 2021 Frank Honolka <slackernetuk@gmail.com>
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

PRGNAM=gmic-gimp
SRCNAM=gmic
VERSION=${VERSION:-3.0.2}
BUILD=${BUILD:-1}
TAG=${TAG:-_snuk}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/snuk}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

GIMP3=${GIMP3:-no}

if [ "$GIMP3" = "yes" ]; then
  GIMP3="gimp3"
else
  GIMP3="gimp"
fi

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=i686 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $SRCNAM-$VERSION
tar xvf $CWD/gmic_$VERSION.tar.gz
cd $SRCNAM-$VERSION
if [ "$GIMP3" == gimp3 ]; then
	zcat $CWD/image_get_active_layer.diff.gz | patch -p1 || exit
fi
cp -a $CWD/cmake . || exit 1
cp $CWD/CMakeLists.txt . || exit 1


chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

cmake -B build -S $TMP/$SRCNAM-$VERSION \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_INSTALL_LIBDIR=/usr/lib$LIBDIRSUFFIX \
	-DCMAKE_INSTALL_MANDIR=/usr/man \
	-DENABLE_DYNAMIC_LINKING=ON \
	-DBUILD_LIB_STATIC=OFF
	cmake --build build

export LDFLAGS="$LDFLAGS -L../build"
cmake -B build-qt -S $TMP/$SRCNAM-$VERSION/gmic-qt \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DENABLE_DYNAMIC_LINKING=ON \
	-DGMIC_PATH=$TMP/$SRCNAM-$VERSION/src \
	-DGMIC_QT_HOST=none
	cmake --build build-qt

cmake -B build-gimp -S $TMP/$SRCNAM-$VERSION/gmic-qt \
	-DCMAKE_INSTALL_PREFIX=/usr \
	-DENABLE_DYNAMIC_LINKING=ON \
	-DGMIC_PATH=$TMP/$SRCNAM-$VERSION/src \
	-DGMIC_QT_HOST=$GIMP3
	cmake --build build-gimp

	 DESTDIR="$PKG" cmake --install build
	 DESTDIR="$PKG" cmake --install build-qt
	 DESTDIR="$PKG" cmake --install build-gimp

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true


find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

find $PKG -name perllocal.pod -o -name ".packlist" -o -name "*.bs" | xargs rm -f || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
DOCS="COPYING README"   

mkdir -p $PKG/usr/share/$PRGNAM || exit 1
cp $TMP/$SRCNAM-$VERSION/resources/gmic_cluts.gmz $PKG/usr/share/$PRGNAM/ || exit 1

cp -a $DOCS $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$PRGNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$PRGNAM.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
/sbin/upgradepkg --install-new --reinstall $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.${PKGTYPE:-txz}
