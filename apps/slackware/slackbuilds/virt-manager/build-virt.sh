#!/bin/bash 
#
src=(	
	osinfo-db-tools 
	osinfo-db 
	libosinfo 
	yajl 
	urlgrabber 
	libvirt 
	libvirt-glib 
	libvirt-python 
	gtk-vnc 
	spice-protocol 
	spice 
	spice-gtk 
	usbredir 
	device-tree-compiler 
	libnfs 
	gtest 
	snappy 
	vde2 
	virglrenderer 
	virt-viewer 
	virt-manager
	qemu	
	)

for i in ${src[@]}; do
	cd source/${i} || exit 1
	sh ${i}.SlackBuild || exit 1
	upgradepkg --install-new --reinstall /tmp/${i}*_snuk.t?z || exit 1
	cd ../.. 
done	
