## renesas_usb_fw.mem


+---------------------+----------------------+----------------------+----------------------+----------------------+
| BIOS boot partition | EFI system partition | Logical volume 1     | Logical volume 2     | Logical volume 3     |
| ef02                | ef00                 |                      |                      |                      |
|                     | /boot/efi            | /                    | [SWAP]               | /home                |
| 5MB                 | 160MB                | 20GB                 | 4GB                  | Rest                 |
|                     |                      | /dev/slack/root      | /dev/slack/swap      | /dev/slack/home      |
| /dev/sda1           | /dev/sda2            |----------------------+----------------------+----------------------+
| unencrypted         | unencrypted          | /dev/sda3 encrypted using LVM on LUKS1  8309                       |
+---------------------+----------------------+--------------------------------------------------------------------+


gdisk or cgdisk to create 3 partitions 

1. size  5MB     type  ef02  
2. size  160MB   type  ef00
3. size  Rest    type  8309 

cryptsetup -s 512 luksFormat --type luks1 /dev/sda3


cryptsetup luksOpen /dev/sda3 slackpv0

pvcreate /dev/mapper/slackpv0
vgcreate slack /dev/mapper/slackpv0
lvcreate -C y -L 4GB -n swap slack
lvcreate -C n -L 20GB -n root slack
lvcreate -C n -l 100%FREE -n home slack

vgscan --mknodes

vgchange -ay

lvscan

mkswap /dev/slack/swap

setup to continue the installation, without lilo/elilo.

#### IMPORTANT ####
Do not reboot when the installation process is finished
#####################

cp -R /etc/resolv.conf /mnt/etc/resolv.conf

cd /mnt 
chroot /mnt /bin/bash -l

dd bs=512 count=4 if=/dev/urandom of=/slackpv.keyfile
cryptsetup luksAddKey /dev/sda3 /slackpv.keyfile
chmod 000 /slackpv.keyfile

mkdir /tmp/initrd-tree
tar xpzf /usr/share/mkinitrd/initrd-tree.tar.gz -C /tmp/initrd-tree

cd /tmp/initrd-tree
wget https://gitlab.com/slackernetuk/slack-desk/-/raw/master/patches/key_file_in_the_initrd_and_drive_unlocked_by_grub.diff

patch init < key_file_in_the_initrd_and_drive_unlocked_by_grub.diff
mv /slackpv.keyfile ./

tar cpzf /usr/share/mkinitrd/initrd-tree.tar.gz *

cd /boot

rm initrd.gz

# create new initrd.gz  
mkinitrd -c -k kernel_version -f ext4 -r /dev/slack/root -m jbd2:mbcache:ext4 -C /dev/sda3 -L -K /slackpv.keyfile -u -o /boot/initrd.gz

# edit /etc/default/grub

GRUB_CMDLINE_LINUX="cryptdevice=UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxx:slackpv0 root=/dev/slack/root cryptkey=/slackpv.keyfile resume=/dev/slack/swap"
GRUB_ENABLE_CRYPTODISK=y

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub --recheck
grub-mkconfig -o /boot/grub/grub.cfg

reboot
