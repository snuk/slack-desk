#!/bin/bash
PRGNAM=xmonad
PRGNAM_HOME=${HOME}/.${PRGNAM}

EPATH=${HOME}/.local/bin

if [ ! -d ${EPATH} ] ; then
mkdir -p ${EPATH}
fi

export PATH=$PATH:$EPATH

CWD=$(pwd)

wget -qO- https://get.haskellstack.org/ | sh

stack setup

mkdir -p ${PRGNAM_HOME} 
cd ${PRGNAM_HOME} 

# from inside of $PRGNAM_HOME/
git clone "https://github.com/xmonad/xmonad" xmonad-git
git clone "https://github.com/xmonad/xmonad-contrib" xmonad-contrib-git
git clone "https://github.com/jaor/xmobar" xmobar-git

# from inside of $PRGNAM_HOME/
stack init

mv ${PRGNAM_HOME}/stack.yaml ${PRGNAM_HOME}/stack.yaml.orig
cp ${CWD}/stack.yaml ${PRGNAM_HOME}/stack.yaml || exit 1

# from inside of $PRGNAM_HOME
stack install

cp ${CWD}/build ${PRGNAM_HOME}/build || exit 1

chmod a+x ${PRGNAM_HOME}/build

cp ${CWD}/xmonad.hs ${PRGNAM_HOME}/ || exit 1
cp ${CWD}/xmobarrc ~/.xmobarrc || exit 1
cp -a ${CWD}/wallpaper ~/wallpaper || exit 1
cp -a ${CWD}/Xresources ~/.Xresources || exit 1

