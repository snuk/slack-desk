#!/bin/bash

for i in \
sphinxcontrib-serializinghtml \
sphinxcontrib-qthelp \
sphinxcontrib-jsmath \
sphinxcontrib-htmlhelp \
sphinxcontrib-devhelp \
sphinxcontrib-applehelp \
snowballstemmer \
MarkupSafe \
Jinja2 \
pytz \
python3-babel \
imagesize \
alabaster \
Sphinx \
imlib2 \
feh \
giblib \
scrot \
xcb-util-xrm \
perl-AnyEvent \
perl-common-sense \
perl-Types-Serialiser \
perl-Canary-Stability \
perl-JSON-XS \
yajl \
libev \
dmenu \
rofi \
i3 \
libmpdclient \
jsoncpp \
siji-font \
gnu-unifont \
lemonbar \
; do
sbopkg -B -i ${i} || exit 1
done

for f in \
	leftwm \
	polybar \
	; do
cd ${f} || exit 1
sh ${f}.SlackBuild || exit 1
cd ..
done


exit 0

xcb-util-xrm
rofi
