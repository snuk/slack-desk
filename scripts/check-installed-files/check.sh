#!/bin/bash


if [ ! -f list.txt ] ; then
	echo  "applications file: list.txt not found"
	exit 1
fi

FILES=`cat list.txt`

for f in $FILES \
; do
ls /var/log/packages/* | grep -w $f >/dev/null 2>&1

if [ ! $? = 0 ]; then
	echo $f >> not-installed.txt 2>&1
else
	echo  $f >> installed.txt 2>&1	
fi
done
